<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    //

    private $name, $backgroud_color, $text_align, $margin, $padding, $designation, $font_color, $gradient;

    /**
     * @return mixed
     */
    public function getGradient()
    {
        return $this->gradient;
    }

    /**
     * @param mixed $gradient
     */
    public function setGradient($gradient)
    {
        $this->gradient = $gradient;
    }

    /**
     * Card constructor.
     * @param $name
     * @param $font_color
     */



    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getBackgroudColor()
    {
        return $this->backgroud_color;
    }

    /**
     * @param mixed $backgroud_color
     */
    public function setBackgroudColor($backgroud_color)
    {
        $this->backgroud_color = $backgroud_color;
    }

    /**
     * @return mixed
     */
    public function getTextAlign()
    {
        return $this->text_align;
    }

    /**
     * @param mixed $text_align
     */
    public function setTextAlign($text_align)
    {
        $this->text_align = $text_align;
    }

    /**
     * @return mixed
     */
    public function getMargin()
    {
        return $this->margin;
    }

    /**
     * @param mixed $margin
     \*/
    public function setMargin($margin)
    {
        $this->margin = $margin;
    }

    /**
     * @return mixed
     */
    public function getPadding()
    {
        return $this->padding;
    }

    /**
     * @param mixed $padding
     */
    public function setPadding($padding)
    {
        $this->padding = $padding;
    }

    /**
     * @return mixed
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * @param mixed $designation
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;
    }

    /**
     * @return mixed
     */
    public function getFontColor()
    {
        return $this->font_color;
    }

    /**
     * @param mixed $font_color
     */
    public function setFontColor($font_color)
    {
        $this->font_color = $font_color;
    }



}
