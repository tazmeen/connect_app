<?php

namespace App\Http\Controllers;

use App\Card;
use Illuminate\Http\Request;

class CardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function show(Card $card)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function edit(Card $card)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Card $card)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function destroy(Card $card)
    {
        //
    }

    public function cards(Request $request){
        $request->input('name');
        $request->input('email');
        $request->input('contact');
        $request->input('site');
        $card = new Card();
        $card_array=array();

        $card->setName("Tazmeen Abdul Jabbar");
        $card->setBackgroudColor("blue");

        $card->setGradient("linear-gradient(#E96874, #6E3663, #2B0830)");


//        array_push($card_array, $card);
        $card_array[]=$card;
        $card=new Card();
        $card->setName("Sheeza Afzal");
        $card->setBackgroudColor("purple");

//        print_r($card_array);
//        array_push($card_array, $card);

        $colours = array();
        array_push($colours, "#525252");
        array_push($colours, "#3d72b4");

        $color_string = 'linear-gradient( ';
        $firstElement = $colours[0];
        foreach($colours as $col)
        {
            if($firstElement==$col){
                $color_string = $color_string . $col;
            }else{
                $color_string = $color_string .', ' . $col;
            }

        }


        $color_string = $color_string . ")";


        $card->setGradient($color_string);

     //   $card->setBackgroudColor($colours);
        $card_array[]=$card;
        return view("display_cards")->with('card_array', $card_array);


    }
}
