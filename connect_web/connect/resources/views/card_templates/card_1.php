<head>

    <style>
        @import 'https://fonts.googleapis.com/css?family=Open+Sans|Roboto:300';

        $padding:30px;

        * { box-sizing: border-box; }


        .card_container{
            width: 40%;
            height: 20%;
            margin-bottom: 4%;


        }

        .card_container_display{
            width: 40%;
            height: 20%;
            margin-bottom: 4%;


        }



        .layout_box_cards{

            margin-top:3%;
            position: relative;
            clear: left;
            left: 0px;
            width: 10%;
            float: left;
        }

        .layout_box_colors{
            position: absolute;
            left: 30%;
            height: 20%;
            clear: left;
            float: left;
            width: auto;
            margin-top: 0px;
        }

        .layout_box_fonts{

        }
        .layout_box_display{
            position: relative;
            display: grid;
            grid-template-columns: auto auto auto;

            left: 30%;
            height: auto;
            clear: left;
            float: left;
            width: auto;
            margin-top: 0px;
        }


        body { background: #757575; overflow-x: hidden; }
        .container {
            perspective: 800px;

            /* Styling */
            color: #fff;
            font-family: 'Open Sans', sans-serif;
            text-transform: uppercase;
            letter-spacing: 4px;

            /* Center it */
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
        .card {
            /* Styling */
            width: 700px;
            height: 400px;
            background: rgb(20,20,20);
            box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);

            /* Card flipping effects */
            transform-style: preserve-3d;
            transition: 0.6s;
        }
        .side {
            position: absolute;
            width: 100%;
            height: 100%;
            backface-visibility: hidden;
            /* Fix Chrome rendering bug */
            transform: rotate(0deg) translateZ(1px);
        }

        /* Flip the card on hover */
        .container:hover .card,
        .back {
            transform: rotateY(-180deg) translateZ(1px);
        }

        /* Front styling */
        .front {
            /* Center the name + outline (almost) */
            line-height: 390px; /* Height - some (because visual center is a little higher than actual center) */
            text-align: center;
        }
        .logo {
            outline: 1px solid #19F6E8;
            display: inline-block;
            padding: 15px 40px;

            text-transform: uppercase;
            font-family: 'Roboto', sans-serif;
            font-size: 1.4em;
            font-weight: normal;
            line-height: 32px;
            letter-spacing: 8px;
        }

        /* Back styling */
        .back {
            background: #15CCC0;
            padding: $padding;
        }
        .name {
            color: #3B3B3B;
            margin-bottom: 0;
        }
        p {
            margin: 0.8em 0;
        }
        .info {
            position: absolute;
            bottom: $padding;
            left: $padding;
            color: #3b3b3b;
        }
        .property {
            color: #fff;
        }

        /* Make semi-responsive */
        @media (max-width:700px) {
            .card { transform: scale(.5); }
            .container:hover .card { transform: scale(.5) rotateY(-180deg) translateZ(1px); }
        }
    </style>


</head>
<!-- The front is based on of my SVG link hover effect https://codepen.io/Zeaklous/pen/kyGqm -->
<body>

@yield('content')
<div class="container">
    <div class="card">

        <div class="front side">
            <h1 class="logo">Zach Saucier</h1>
        </div>

        <div class="back side">
            <h3 class="name">{{$card_array[0]->getName()}}</h3>
            <div>Front-end developer</div>
            <div class="info">
                <p><span class="property">Email: </span>hello@zachsaucier.com</p>
                <p><span class="property">Twitter: </span>@ZachSaucier</p>
                <p><span class="property">Phone: </span>(123) 456-7890</p>
                <p><span class="property">Website: </span>zachsaucier.com</p>
            </div>
        </div>

    </div>
</div>

</body>

