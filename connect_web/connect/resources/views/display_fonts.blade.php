<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>

        <link rel="stylesheet"
              href="https://fonts.googleapis.com/css?family=Tangerine">

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <title>Cards</title>


        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href='https://fonts.googleapis.com/css?family=Aclonica' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Bad Script' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Astloch' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Atma' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Balthazar' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Barrio' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Balthazar' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Annie Use Your Telescope' rel='stylesheet'>


        <style>

            .layout{

                display: grid;
                grid-template-columns: auto auto auto auto;
                margin-left: 7%;
            }

            .module{
                display: grid;
                margin: 2%;
            }

            .color-tag{
                display: grid;
                height: auto;
                width: auto;


            }

            <?php

            $fonts = array();
            array_push($fonts, "'Times New Roman', Times, serif");
            array_push($fonts, "'Palatino Linotype', 'Book Antiqua', Palatino, serif");
            array_push($fonts, "Arial, Helvetica, sans-serif");
            array_push($fonts, "'Comic Sans MS', cursive, sans-serif");
            array_push($fonts, "'Lucida Sans Unicode', 'Lucida Grande', sans-serif");
            array_push($fonts, "'Courier New', Courier, monospace");
            array_push($fonts, "'Tangerine', serif");
            array_push($fonts, "'Comic Sans MS', cursive, sans-serif");
            array_push($fonts, "Astloch");
            array_push($fonts, "Barrio");
            array_push($fonts, "Atma");
            array_push($fonts, "Annie Use Your Telescope");
            //$fonts.push();

            ?>

            .font{


            }

            h1 { color: #212121; font-family: 'Lato', sans-serif; font-size: 54px; font-weight: 200; line-height: 58px; margin: 2% 2% 2% 30%; }



        </style>

        <script>
            $(document).ready(function(){
                $(".module").click(function(){
//                    window.alert(this.textContent);

                    $( this ).fadeTo(500, 0.2);
                });
            });
        </script>



    </head>

       <body>


       <h1>Your Business Card</h1>


        <div class="layout">



                @foreach($fonts as $font)
                    <div class="module"><div style="font-family: {{$font}}" >
                    <h2>Pick a font</h2>
                </div>
                    </div>
                @endforeach



        </div>



       <br>

       <div style="margin-left: 47%;">
    <form action="/fonts" method="get">
        <input type="submit" name="next">
    </form>
       </div>


    </body>

</html>
