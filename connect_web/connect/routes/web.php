<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('my_details');
});

Route::get('/colors', function () {

    return view('display_colors');
});

Route::get('/fonts', function () {

    return view('display_fonts');
});

Route::post('/post_form', 'CardController@cards');


Route::get('/cards', 'CardController@cards');
