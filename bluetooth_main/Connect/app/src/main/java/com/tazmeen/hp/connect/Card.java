package com.tazmeen.hp.connect;

import android.graphics.Color;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by hp on 12/9/2017.
 */

public class Card implements Serializable {

    private Text[] txt;

    private Image[] img;
    private String cardID;


    public Card(Text text[], String card_background_color){
        //String r = "RED";
        this.txt = text;

        int color = Color.parseColor(card_background_color);

        String cardID = UUID.randomUUID().toString();





        // int color = Color.argb(randomNum, randomNum1, randomNum2, randomNum3);
        this.color_fill=color;
       // Log.i("color", String.valueOf(this.color_fill));
    }

    /*public Card(String name, String value) {
        this.name = name;
        this.value = value;

        int randomNum = 1 + (int)(Math.random() * 255);
        int randomNum1 = 1 + (int)(Math.random() * 255);
        int randomNum2 = 1 + (int)(Math.random() * 255);
        int randomNum3 = 1 + (int)(Math.random() * 255);
        int color = Color.argb(randomNum, randomNum1, randomNum2, randomNum3);
        // int c = Color.BLACK;
        this.color_fill=color;
       // String c = Integer.toString(color);
    }
*/
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private String value;



    private int color_fill;

    public int getColor_fill() {
        return color_fill;
    }

    public Text[] getTxt() {
        return txt;
    }

    public Text getText(int x) {
        return txt[x];
    }

    public void setTxt(Text[] txt) {
        this.txt = txt;
    }

    public Image[] getImg() {
        return img;
    }

    public void setImg(Image[] img) {
        this.img = img;
    }

    public void setColor_fill(int color_fill) {
        this.color_fill = color_fill;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getColor_border() {
        return color_border;
    }

    public void setColor_border(String color_border) {
        this.color_border = color_border;
    }

    private int ID;
    private String color_border;



}
