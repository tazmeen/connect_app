package com.tazmeen.hp.connect.firebase;

import com.tazmeen.hp.connect.listeners.IFireBaseRealTimeListener;
import com.tazmeen.hp.connect.models.EmployeeCustomer;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

/**
 * Created by Ansar Shahid on 3/17/2018.
 */

public class FireBaseRealTimeListenerHelper implements ChildEventListener {

    private final IFireBaseRealTimeListener iFireBaseRealTimeListener;

    public FireBaseRealTimeListenerHelper(IFireBaseRealTimeListener iFireBaseRealTimeListener) {
        this.iFireBaseRealTimeListener = iFireBaseRealTimeListener;
    }

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        EmployeeCustomer employeeCustomer = dataSnapshot.getValue(EmployeeCustomer.class);
        iFireBaseRealTimeListener.onChildAdded(employeeCustomer);
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }
}
