package com.tazmeen.hp.connect;

import android.bluetooth.BluetoothAdapter;

/**
 * Created by hp on 3/20/2018.
 */


    public class Globals{
        private static Globals instance;

        // Global variable
        public BluetoothConnectionService mBluetoothConnection;

        // Restrict the constructor from being instantiated
        private Globals(){}

    public BluetoothConnectionService getmBluetoothAdapter() {
        return mBluetoothConnection;
    }

    public void setmBluetoothAdapter(BluetoothConnectionService mBluetoothConnection) {
        this.mBluetoothConnection = mBluetoothConnection;
    }

        public static synchronized Globals getInstance(){
            if(instance==null){
                instance=new Globals();
            }
            return instance;
        }

}

