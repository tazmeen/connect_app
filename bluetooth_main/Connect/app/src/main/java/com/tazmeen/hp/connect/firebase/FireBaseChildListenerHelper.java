package com.tazmeen.hp.connect.firebase;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.tazmeen.hp.connect.models.Cards;
import com.tazmeen.hp.connect.models.Customer;

/**
 * Created by hp on 3/18/2018.
 */

public class FireBaseChildListenerHelper  implements ChildEventListener {
    private IFirebaseChildClassListener iFirebaseChildClassListener;

    public FireBaseChildListenerHelper(IFirebaseChildClassListener iFirebaseChildClassListener) {
        this.iFirebaseChildClassListener = iFirebaseChildClassListener;
    }
    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        Cards card = dataSnapshot.getValue(Cards.class);
        iFirebaseChildClassListener.onChildAdded(card);
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }
}
