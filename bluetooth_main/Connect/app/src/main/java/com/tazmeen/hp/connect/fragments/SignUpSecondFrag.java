package com.tazmeen.hp.connect.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.craftman.cardform.CardForm;
import com.tazmeen.hp.connect.R;
import com.tazmeen.hp.connect.models.SignUpModel;
import com.google.gson.Gson;

/**
 * Created by Ansar Shahid on 3/16/2018.
 */


public class SignUpSecondFrag extends Fragment implements View.OnClickListener {

    private CardForm cardForm;
    private TextView txtDea;
    private Button btnPay;
    private SignUpModel signUpModel;
    private ISecondFragListener secondFragListener;

    public SignUpSecondFrag() {

    }

    public static SignUpSecondFrag getInstance() {
        return new SignUpSecondFrag();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context != null)
            if (context instanceof ISecondFragListener)
                secondFragListener = (ISecondFragListener) context;
            else
                throw new RuntimeException("Activity does not implement ISecondFragListener");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_up_second, container, false);
        signUpModel = new Gson().fromJson(getArguments().getString("signUpModel"), SignUpModel.class);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        cardForm = view.findViewById(R.id.cardform);
        TextView txtDea = view.findViewById(R.id.payment_amount);
        Button btnPay = view.findViewById(R.id.btn_pay);
        btnPay.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        secondFragListener.onSubmitClicked(signUpModel);
    }

    @FunctionalInterface
    public interface ISecondFragListener {
        void onSubmitClicked(SignUpModel signUpModel);
    }
}
