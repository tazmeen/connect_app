package com.tazmeen.hp.connect.utils;

import android.content.Context;
import android.content.SharedPreferences;

import static com.tazmeen.hp.connect.Constants.ADMIN_ROLE;

/**
 * Created by Ansar Shahid on 3/17/2018.
 */

public class SharedPreferneceHelper {

    public static final String SHARED_PREFERENCE = "com.tazmeen.hp.connect.shared_preference";
    private static SharedPreferneceHelper sharedPreferneceHelper = null;
    private SharedPreferences sharedPreferences;

    private SharedPreferneceHelper(Context context) {
        sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCE, Context.MODE_PRIVATE);
    }

    public static SharedPreferneceHelper getInstance(Context context) {
        if (sharedPreferneceHelper == null)
            sharedPreferneceHelper = new SharedPreferneceHelper(context);
        return sharedPreferneceHelper;
    }

    public void putString(String key, String value) {
        sharedPreferences.edit().putString(key, value).apply();
    }

    public String getString(String key) {
        return sharedPreferences.getString(key, "");
    }

    public void putBoolean(String key, boolean value) {
        sharedPreferences.edit().putBoolean(key, value).apply();
    }

    public boolean getBoolean(String key) {
        return sharedPreferences.getBoolean(key, ADMIN_ROLE);
    }

    public void clear() {
        sharedPreferences.edit().clear().apply();
    }
}
