package com.tazmeen.hp.connect.firebase;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.tazmeen.hp.connect.models.Customer;

/**
 * Created by hp on 3/18/2018.
 */

public class FireBaseCardRealTimeHelper {
    private static final String CUSTOMERS = "customers";
//    public static final String EMPLOYEES = "employees";
    private final DatabaseReference customerDatabaseReference;
//    private final DatabaseReference employeeDatabaseReference;
    private FirebaseAuth mAuth;

    public FireBaseCardRealTimeHelper(FirebaseAuth mAuth) {
        customerDatabaseReference = FirebaseDatabase.getInstance().getReference().child(CUSTOMERS).child(mAuth.getCurrentUser().getUid()).child("cards");
//        employeeDatabaseReference = FirebaseDatabase.getInstance().getReference().child(EMPLOYEES);
    }

    public void pushData(Customer customer) {
        customerDatabaseReference.child(customer.getUuid())
                .setValue(customer);
    }

//    public void pushData(Employee employee) {
//        employeeDatabaseReference.child(employee.uuid())
//                .setValue(employee);
//    }
}
