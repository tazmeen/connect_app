package com.tazmeen.hp.connect;

import java.io.Serializable;

/**
 * Created by hp on 12/9/2017.
 */

public class Text implements Serializable {
    private float x;
    private float y;



    public Text(String key, String value) {
        setText(key, value);
    }


    public void setText(String key, String value) {
        this.key = key;
        this.value = value;
    }


    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    private String color;
    private String key;
    private String value;
    private int size;
}
