package com.tazmeen.hp.connect.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.tazmeen.hp.connect.R;
import com.tazmeen.hp.connect.listeners.IItemClickListener;

/**
 * Created by hp on 3/19/2018.
 */

public class CardViewHolder extends RecyclerView.ViewHolder {

    private ImageView singleCardImageView;
    private IItemClickListener iItemClickListener;

    public CardViewHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                iItemClickListener.onItemClick(getAdapterPosition());
            }
        });
        singleCardImageView = itemView.findViewById(R.id.singleCardImageView);
    }

    public ImageView getSingleCardImageView() {
        return singleCardImageView;
    }

    public void setItemClickListener(IItemClickListener iItemClickListener) {
        this.iItemClickListener = iItemClickListener;
    }

//    public void bind(final IItemClickListener listener) {
//
//        itemView.setOnClickListener(new View.OnClickListener() {
//            @Override public void onClick(View v) {
//                listener.onItemClick(getAdapterPosition());
//            }
//        });
//    }






}
