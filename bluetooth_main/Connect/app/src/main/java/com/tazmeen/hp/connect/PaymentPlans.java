package com.tazmeen.hp.connect;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class PaymentPlans extends AppCompatActivity {

    Button btnPlan1,btnPlan2,btnPlan3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_plans);
        btnPlan1=(Button)findViewById(R.id.btnPlan1);
        btnPlan2=(Button)findViewById(R.id.btnPlan2);
        btnPlan3=(Button)findViewById(R.id.btnPlan3);

        btnPlan1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer amount = 500;
                Intent intent = new Intent(PaymentPlans.this, SignUpActivity.class);
                intent.putExtra("amount",amount);
                startActivity(intent);

            }
        });
        btnPlan2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        btnPlan3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });





    }
}
