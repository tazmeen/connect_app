package com.tazmeen.hp.connect.listeners;

/**
 * Created by Ansar Shahid on 3/17/2018.
 */

@FunctionalInterface
public interface IItemClickListener {

    void onItemClick(int position);
}
