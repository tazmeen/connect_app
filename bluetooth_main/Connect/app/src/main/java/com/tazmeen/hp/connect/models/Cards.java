package com.tazmeen.hp.connect.models;


/**
 * Created by hp on 3/18/2018.
 */

public class Cards {

    private String address;
    private String card_ID;
    private String customer_UUID;
    private String email;
    private String name;
    private String number;
    private String uri;

    public Cards(String address, String card_ID, String customer_UUID, String email, String name, String number, String uri) {
        this.address = address;
        this.card_ID = card_ID;
        this.customer_UUID = customer_UUID;
        this.email = email;
        this.name = name;
        this.number = number;
        this.uri = uri;
    }

    public Cards() {
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCard_ID() {
        return card_ID;
    }

    public void setCard_ID(String card_ID) {
        this.card_ID = card_ID;
    }

    public String getCustomer_UUID() {
        return customer_UUID;
    }

    public void setCustomer_UUID(String customer_UUID) {
        this.customer_UUID = customer_UUID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    @Override
    public String toString() {
        return "Cards{" +
                "address='" + address + '\'' +
                ", card_ID='" + card_ID + '\'' +
                ", customer_UUID='" + customer_UUID + '\'' +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", number='" + number + '\'' +
                ", uri='" + uri + '\'' +
                '}';
    }
}
