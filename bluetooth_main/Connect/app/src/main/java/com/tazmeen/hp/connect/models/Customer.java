package com.tazmeen.hp.connect.models;

import android.graphics.Bitmap;

/**
 * Created by Ansar Shahid on 3/16/2018.
 */

public class Customer {

    private String uuid;
    private String name;
    private String phoneNumber;
//    private Int Card_ID
    private ReceivedCards[] rec_cards;
    private String email;
    private boolean customerRole;

    public Customer(String uuid, String name, String phoneNumber, String email, boolean customerRole) {
        this.uuid = uuid;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.customerRole = customerRole;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isCustomerRole() {
        return customerRole;
    }

    public void setCustomerRole(boolean customerRole) {
        this.customerRole = customerRole;
    }
}
