package com.tazmeen.hp.connect;

import com.tazmeen.hp.connect.models.Cards;

/**
 * Created by Ansar Shahid on 3/16/2018.
 */

public class Constants {

    public static final boolean ADMIN_ROLE = true;
    public static final String USER_NAME = "user_name";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_ROLE = "user_role";
    public static final String USER_PHONE = "user_phone";
    public static final String USER_COMPANY_NAME = "user_company_name";
    public static final String CUSTOMERS = "customers";



}
