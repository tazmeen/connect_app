package com.tazmeen.hp.connect.firebase;

import com.tazmeen.hp.connect.models.Cards;

/**
 * Created by hp on 3/19/2018.
 */

@FunctionalInterface
public interface IFireBaseCardReadListener {

    void onCardAdded(Cards cards);
}
