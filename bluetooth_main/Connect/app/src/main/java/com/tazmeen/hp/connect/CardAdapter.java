package com.tazmeen.hp.connect;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.tazmeen.hp.connect.viewHolders.CardViewHolder;

import java.util.ArrayList;

/**
 * Created by hp on 12/9/2017.
 */

public class CardAdapter extends ArrayAdapter<Card> {


    private LayoutInflater mLayoutInflater;
    private ArrayList<Card> Cards;
    private int  mViewResourceId;
    private static LayoutInflater inflater = null;
    public  CardView cView;

    public CardAdapter(Context context, int tvResourceId, ArrayList<Card> devices){
        super(context, tvResourceId, devices);
        this.Cards = devices;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mViewResourceId = tvResourceId;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mLayoutInflater.inflate(mViewResourceId, null);

        Card device = Cards.get(position);


        if (device != null) {
            TextView cName = (TextView) convertView.findViewById(R.id.cardName);
            TextView cPhone = (TextView) convertView.findViewById(R.id.cardPhone);
            TextView cAddress = (TextView) convertView.findViewById(R.id.cardAddress);
            TextView cEmail = (TextView) convertView.findViewById(R.id.cardEmail);
             cView = (CardView) convertView.findViewById(R.id.cardView);
             cView.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Log.i("TAG", "onClick: kyaa haaal hai");
                 }
             });

            if (cName != null) {
                cName.setText(device.getText(0).getValue());
            }
            if (cPhone != null) {
                cPhone.setText(device.getText(1).getValue());
            }
            if (cEmail != null) {
                cEmail.setText(device.getText(2).getValue());
            }
            if (cAddress != null) {
                cAddress.setText(device.getText(3).getValue());
            }
            if(cView != null){
               /* int randomNum = 1 + (int)(Math.random() * 255);
                int randomNum1 = 1 + (int)(Math.random() * 255);
                int randomNum2 = 1 + (int)(Math.random() * 255);
                int randomNum3 = 1 + (int)(Math.random() * 255);
                int color = Color.argb(randomNum, randomNum1, randomNum2, randomNum3);
               // int c = Color.BLACK;
                String c = Integer.toString(color);
                Log.d(TAG, c);*/

               //cView.
               cView.setCardBackgroundColor(device.getColor_fill());
            }
        }


        return convertView;
    }



}
