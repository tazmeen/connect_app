package com.tazmeen.hp.connect;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.tazmeen.hp.connect.adapter.AddUserAdapter;
import com.tazmeen.hp.connect.firebase.FireBaseRealTimeListenerHelper;
import com.tazmeen.hp.connect.listeners.IFireBaseRealTimeListener;
import com.tazmeen.hp.connect.models.EmployeeCustomer;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import static com.tazmeen.hp.connect.firebase.FireBaseRealTimeHelper.EMPLOYEES;

/**
 * Created by Ansar Shahid on 3/17/2018.
 */

public class AddUserActivity extends AppCompatActivity implements IFireBaseRealTimeListener {

    private List<EmployeeCustomer> customerList = new ArrayList<>();
    private AddUserAdapter addUserAdapter;
    private DatabaseReference firebaseDatabase;
    private FireBaseRealTimeListenerHelper realTimeListenerHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
        RecyclerView addUserRecyclerView = findViewById(R.id.addUserRecyclerView);
        addUserRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        addUserRecyclerView.setHasFixedSize(true);
        addUserAdapter = new AddUserAdapter(customerList, this);
        addUserRecyclerView.setAdapter(addUserAdapter);
        realTimeListenerHelper = new FireBaseRealTimeListenerHelper(this);
        firebaseDatabase = FirebaseDatabase.getInstance().getReference().child(EMPLOYEES);
        firebaseDatabase.addChildEventListener(realTimeListenerHelper);
    }

    @Override
    public void onChildAdded(EmployeeCustomer employeeCustomer) {
        customerList.add(employeeCustomer);
        if (addUserAdapter != null)
            addUserAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (realTimeListenerHelper != null && firebaseDatabase != null)
            firebaseDatabase.removeEventListener(realTimeListenerHelper);
        realTimeListenerHelper = null;
        firebaseDatabase = null;
        addUserAdapter = null;
        if (customerList != null)
            customerList.clear();
        customerList = null;
    }
}
