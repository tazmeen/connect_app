package com.tazmeen.hp.connect.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.tazmeen.hp.connect.R;
import com.tazmeen.hp.connect.models.SignUpModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpFirstFrag extends Fragment {

    private EditText etName, etUser, etPhone, etEmail, etPass, etConfirmPass;

    public SignUpFirstFrag() {
        // Required empty public constructor
    }

    public static SignUpFirstFrag getInstance() {
        return new SignUpFirstFrag();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_up_first, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        etUser = view.findViewById(R.id.etUser);
        etPhone = view.findViewById(R.id.etPhone);
        etEmail = view.findViewById(R.id.etEmail);
        etPass = view.findViewById(R.id.etPass);
        etConfirmPass = view.findViewById(R.id.etConfirmPass);
        etName = view.findViewById(R.id.etName);
    }

    public boolean checkEditText() {
        if (TextUtils.isEmpty(etName.getText().toString())) {
            etName.setError("Name cannot be empty!");
            return false;
        } else if (TextUtils.isEmpty(etPhone.getText().toString())) {
            etPhone.setError("Phone Number cannot be empty!");
            return false;
        } else if (TextUtils.isEmpty(etEmail.getText().toString())) {
            etEmail.setError("Email cannot be empty!");
            return false;
        } else if (TextUtils.isEmpty(etUser.getText().toString())) {
            etUser.setError("Username cannot be empty!");
            return false;
        } else if (TextUtils.isEmpty(etPass.getText().toString())) {
            etPass.setError("Password cannot be empty!");
            return false;
        } else if (TextUtils.isEmpty(etConfirmPass.getText().toString())) {
            etConfirmPass.setError("Confirm Password cannot be empty!");
            return false;
        }else if(!TextUtils.equals(etPass.getText().toString(),etConfirmPass.getText().toString())){
            etConfirmPass.setError("Confirm password not same!");
            return false;
        }
        return true;
    }

    public SignUpModel getSignUpModel(){
        return new SignUpModel(etName.getText().toString(),etUser.getText().toString(),etEmail.getText().toString(),etPass.getText().toString(),etPhone.getText().toString());
    }
}