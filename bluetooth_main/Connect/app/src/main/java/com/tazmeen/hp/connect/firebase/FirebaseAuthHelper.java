package com.tazmeen.hp.connect.firebase;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.tazmeen.hp.connect.listeners.IFireBaseAuthListener;
import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by Ansar Shahid on 3/16/2018.
 */

public class FirebaseAuthHelper {

    private final IFireBaseAuthListener iFireBaseAuthListener;

    public FirebaseAuthHelper(final IFireBaseAuthListener iFireBaseAuthListener) {
        this.iFireBaseAuthListener = iFireBaseAuthListener;
    }

    public void createAccount(final String email, final String password) {
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        iFireBaseAuthListener.onAuthSuccessful(task);
                    }
                });
    }
}
