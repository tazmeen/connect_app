package com.tazmeen.hp.connect;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.tasks.Task;
import com.tazmeen.hp.connect.UiHelper;
import com.tazmeen.hp.connect.firebase.FireBaseRealTimeHelper;
import com.tazmeen.hp.connect.firebase.FirebaseAuthHelper;
import com.tazmeen.hp.connect.fragments.SignUpFirstFrag;
import com.tazmeen.hp.connect.fragments.SignUpSecondFrag;
import com.tazmeen.hp.connect.listeners.IFireBaseAuthListener;
import com.tazmeen.hp.connect.models.Customer;
import com.tazmeen.hp.connect.models.SignUpModel;
import com.tazmeen.hp.connect.utils.SharedPreferneceHelper;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;

import static com.tazmeen.hp.connect.Constants.ADMIN_ROLE;
import static com.tazmeen.hp.connect.Constants.USER_COMPANY_NAME;
import static com.tazmeen.hp.connect.Constants.USER_EMAIL;
import static com.tazmeen.hp.connect.Constants.USER_NAME;
import static com.tazmeen.hp.connect.Constants.USER_PHONE;
import static com.tazmeen.hp.connect.Constants.USER_ROLE;


public class SignUpActivity extends AppCompatActivity implements SignUpSecondFrag.ISecondFragListener {

    private SignUpFirstFrag signUpFirstFrag;
    private SignUpSecondFrag signUpSecondFrag;
    private Button btnSubmit;
    private int counter = 0;
    private MaterialDialog materialDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up_activity);
        if (!SharedPreferneceHelper.getInstance(this).getString(USER_EMAIL).isEmpty()) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            this.finish();
        }
        setFragmentInstance();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().add(R.id.signUpFragHolder, signUpFirstFrag).commit();
        counter++;
        btnSubmit = findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (signUpFirstFrag.checkEditText()) {
                    SignUpModel signUpModel = signUpFirstFrag.getSignUpModel();
                    Bundle bundle = new Bundle();
                    bundle.putString("signUpModel", new Gson().toJson(signUpModel));
                    signUpSecondFrag.setArguments(bundle);
                    animateRightToLeft(signUpSecondFrag);
                    btnSubmit.setVisibility(View.INVISIBLE);
                    counter++;
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (counter == 2) {
            animateLeftToRight(signUpFirstFrag);
            btnSubmit.setVisibility(View.VISIBLE);
            counter--;
        } else if (counter == 1)
            super.onBackPressed();
    }

    private void setFragmentInstance() {
        signUpFirstFrag = SignUpFirstFrag.getInstance();
        signUpSecondFrag = SignUpSecondFrag.getInstance();
    }

    private void animateLeftToRight(Fragment fragment) {
        try {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
            transaction.replace(R.id.signUpFragHolder, fragment);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void animateRightToLeft(Fragment fragment) {
        try {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
            transaction.replace(R.id.signUpFragHolder, fragment);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        signUpFirstFrag = null;
        signUpSecondFrag = null;
        materialDialog = null;
    }

    @Override
    public void onSubmitClicked(final SignUpModel signUpModel) {
        if (materialDialog == null)
            materialDialog = UiHelper.getInstance().getCircularProgress(this, "Creating Account..");
        else
            materialDialog.show();
        new FirebaseAuthHelper(new IFireBaseAuthListener() {
            @Override
            public void onAuthSuccessful(Task task) {
                materialDialog.dismiss();
                if (task.isSuccessful()) {
                    String uuid = FirebaseAuth.getInstance().getCurrentUser().getUid();
                    new FireBaseRealTimeHelper().pushData(new Customer(uuid, signUpModel.getCompanyName(), signUpModel.getPhone(), signUpModel.getEmail(), ADMIN_ROLE));
                    SharedPreferneceHelper sharedPreferneceHelper = SharedPreferneceHelper.getInstance(SignUpActivity.this);
                    sharedPreferneceHelper.putString(USER_NAME, signUpModel.getUsername());
                    sharedPreferneceHelper.putString(USER_EMAIL, signUpModel.getEmail());
                    sharedPreferneceHelper.putString(USER_COMPANY_NAME, signUpModel.getCompanyName());
                    sharedPreferneceHelper.putString(USER_PHONE, signUpModel.getPhone());
                    sharedPreferneceHelper.putBoolean(USER_ROLE, ADMIN_ROLE);
                    Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                } else
                    Toast.makeText(SignUpActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
        ).createAccount(signUpModel.getEmail(), signUpModel.getPassword());
    }
}
