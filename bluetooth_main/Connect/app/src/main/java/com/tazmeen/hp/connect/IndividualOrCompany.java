package com.tazmeen.hp.connect;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class IndividualOrCompany extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_individual_or_company);

        ImageButton ib = (ImageButton) findViewById(R.id.imageButton7);
        ib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(IndividualOrCompany.this, SignUpActivity.class);
                startActivity(intent);
            }
        });


    }
}
