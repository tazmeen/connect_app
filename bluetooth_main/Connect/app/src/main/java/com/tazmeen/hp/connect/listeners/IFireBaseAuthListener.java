package com.tazmeen.hp.connect.listeners;

import com.google.android.gms.tasks.Task;

/**
 * Created by Ansar Shahid on 3/16/2018.
 */

@FunctionalInterface
public interface IFireBaseAuthListener {

    void onAuthSuccessful(Task task);
}
