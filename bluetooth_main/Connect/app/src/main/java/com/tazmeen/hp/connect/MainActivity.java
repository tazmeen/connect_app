package com.tazmeen.hp.connect;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;
import com.tazmeen.hp.connect.app.MyApplication;
import com.tazmeen.hp.connect.firebase.CardReadListeneerHelper;
import com.tazmeen.hp.connect.firebase.IFireBaseCardReadListener;
import com.tazmeen.hp.connect.listeners.IItemClickListener;
//import com.tazmeen.hp.connect.listeners.RecyclerItemClickListener;
import com.tazmeen.hp.connect.models.Cards;
import com.tazmeen.hp.connect.utils.SharedPreferneceHelper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


import static com.tazmeen.hp.connect.Constants.USER_NAME;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, AdapterView.OnItemClickListener {

    private static final String TAG = "MainActivity";
    private static final String CUSTOMERS = "customers";
    public FirebaseAuth mAuth;
    Globals g = Globals.getInstance();

    RecyclerView listNames;

    ListView wallet;
//    CardView received_card = (CardView) findViewById(R.id.item_base_lang);
    //public ArrayList<String> names;

    public final ArrayList<String> nameList = new ArrayList<String>();
    public final static ArrayList<Card> cardList = new ArrayList<Card>();
    public final static ArrayList<Card> walletList = new ArrayList<Card>();
    public static FragmentManager fragmentManager;
    public CardAdapter cardAdapter;
    private List<Cards> cardsList;

    public BluetoothAdapter mBluetoothAdapter;



    Button btnEnableDisable_Discoverable;
    BluetoothConnectionService mBluetoothConnection;

    public BluetoothConnectionService getmBluetoothConnection() {
        return mBluetoothConnection;
    }

    public void setmBluetoothConnection(BluetoothConnectionService mBluetoothConnection) {
        this.mBluetoothConnection = mBluetoothConnection;
    }

    Button btnStartConnection;
    //   Button btnSend;
    Button btnSend2;

    StringBuilder messages;


    //EditText etSend;
    EditText etSend2;


    private static final UUID MY_UUID_INSECURE =
            UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a66");

    BluetoothDevice mBTDevice;

    public ArrayList<BluetoothDevice> mBTDevices = new ArrayList<>();

    public DeviceListAdapter mDeviceListAdapter;
    public CardAdapter cardListAdapter;
    private com.tazmeen.hp.connect.adapter.CardAdapter myCardAdapter;
    public CardAdapter walletAdapter;

    ListView lvNewDevices;
    // Create a BroadcastReceiver for ACTION_FOUND
    private final BroadcastReceiver mBroadcastReceiver1 = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            // When discovery finds a device
            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);

                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        Log.d(TAG, "onReceive: STATE OFF");
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        Log.d(TAG, "mBroadcastReceiver1: STATE TURNING OFF");
                        break;
                    case BluetoothAdapter.STATE_ON:
                        Log.d(TAG, "mBroadcastReceiver1: STATE ON");
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        Log.d(TAG, "mBroadcastReceiver1: STATE TURNING ON");
                        break;
                }
            }
        }
    };

    /**
     * Broadcast Receiver for changes made to bluetooth states such as:
     * 1) Discoverability mode on/off or expire.
     */
    private final BroadcastReceiver mBroadcastReceiver2 = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED)) {

                int mode = intent.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE, BluetoothAdapter.ERROR);

                switch (mode) {
                    //Device is in Discoverable Mode
                    case BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE:
                        Log.d(TAG, "mBroadcastReceiver2: Discoverability Enabled.");
                        break;
                    //Device not in discoverable mode
                    case BluetoothAdapter.SCAN_MODE_CONNECTABLE:
                        Log.d(TAG, "mBroadcastReceiver2: Discoverability Disabled. Able to receive connections.");
                        break;
                    case BluetoothAdapter.SCAN_MODE_NONE:
                        Log.d(TAG, "mBroadcastReceiver2: Discoverability Disabled. Not able to receive connections.");
                        break;
                    case BluetoothAdapter.STATE_CONNECTING:
                        Log.d(TAG, "mBroadcastReceiver2: Connecting....");
                        break;
                    case BluetoothAdapter.STATE_CONNECTED:
                        Log.d(TAG, "mBroadcastReceiver2: Connected.");
                        break;
                }

            }
        }
    };


    /**
     * Broadcast Receiver for listing devices that are not yet paired
     * -Executed by btnDiscover() method.
     */
    private BroadcastReceiver mBroadcastReceiver3 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            Log.d(TAG, "onReceive: ACTION FOUND.");
            Log.d("here", "taz");


            if (action.equals(BluetoothDevice.ACTION_FOUND)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                mBTDevices.add(device);
                Log.d(TAG, "onReceive: " + device.getName() + ": " + device.getAddress());
                mDeviceListAdapter = new DeviceListAdapter(context, R.layout.device_adapter_view, mBTDevices);
                lvNewDevices.setAdapter(mDeviceListAdapter);
            }
        }
    };

    /**
     * Broadcast Receiver that detects bond state changes (Pairing status changes)
     */
    private final BroadcastReceiver mBroadcastReceiver4 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothDevice.ACTION_BOND_STATE_CHANGED)) {
                BluetoothDevice mDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                //3 cases:
                //case1: bonded already
                if (mDevice.getBondState() == BluetoothDevice.BOND_BONDED) {
                    Log.d(TAG, "BroadcastReceiver: BOND_BONDED.");
                    //inside BroadcastReceiver4
                    mBTDevice = mDevice;
                }
                //case2: creating a bone
                if (mDevice.getBondState() == BluetoothDevice.BOND_BONDING) {
                    Log.d(TAG, "BroadcastReceiver: BOND_BONDING.");
                }
                //case3: breaking a bond
                if (mDevice.getBondState() == BluetoothDevice.BOND_NONE) {
                    Log.d(TAG, "BroadcastReceiver: BOND_NONE.");
                }
            }
        }
    };

    //  @Override
    protected void onDestroyBluetooth() {
        Log.d(TAG, "onDestroy: called.");
        super.onDestroy();
        unregisterReceiver(mBroadcastReceiver1);
        unregisterReceiver(mBroadcastReceiver2);
        unregisterReceiver(mBroadcastReceiver3);
        unregisterReceiver(mBroadcastReceiver4);
        //mBluetoothAdapter.cancelDiscovery();
    }
//    Button btnONOFF = (Button) findViewById(R.id.btnONOFF);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


//        mAuth = FirebaseAuth.getInstance();//auth aa ra hai ab let me see
//        Log.i("Tag", mAuth.getCurrentUser().getUid());
        CheckLoginStatus();
        cardsList = new ArrayList<>();
        listNames = findViewById(R.id.listNames);
        listNames.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        listNames.setHasFixedSize(true);
        Picasso picasso = ((MyApplication) getApplicationContext()).getPicasso();
        myCardAdapter = new com.tazmeen.hp.connect.adapter.CardAdapter(cardsList, this, picasso);
        listNames.setAdapter(myCardAdapter);
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        if (firebaseUser != null) {
            Log.e(TAG, firebaseUser.getUid());
            databaseReference.child("cards").addChildEventListener(new CardReadListeneerHelper(new IFireBaseCardReadListener() {
                @Override
                public void onCardAdded(Cards cards) {
                    cardsList.add(cards);
                    Log.e(TAG, cards.toString());
                    myCardAdapter.notifyDataSetChanged();
                }
            }));
        } else {
            Log.e(TAG, "FireBase User is null");
        }

        myCardAdapter.setBluetoothConnectionService(mBluetoothConnection);


        Text text_phone = new Text("Phone", "03004823843");
        Text text_email = new Text("Email", "tazmeen@live.com");
        Text text_address = new Text("Address", "43 D Model Town");


        Text[] text_sample = new Text[4];
        text_sample[0] = new Text("Name", "Tazmeen");
        text_sample[1] = text_phone;
        text_sample[2] = text_email;
        text_sample[3] = text_address;

        Card card_samples = new Card(text_sample, "teal");
        // cardList.add(card_samples);
        Log.i("name", text_email.getValue());

        text_phone = new Text("Phone", "0423567578");
        text_email = new Text("Email", "sheeza@live.com");
        text_address = new Text("Address", "3 E Faisal Town");

        //Text[] text_sample = new Text[4];
        text_sample[0] = new Text("Name", "Tazmeen");
        text_sample[1] = text_phone;
        text_sample[2] = text_email;
        text_sample[3] = text_address;
        Log.i("name", text_email.getValue());

        Card card_sample = new Card(text_sample, "lime");
//        cardList.add(card_sample);


        // Card card1 = ;

        //cardList.add((Card) intent_add_card.getSerializableExtra("mycard"));
        //Log.d("name", "phone");


        //    Card card1 = new Card("Name", "Tazmeen");
        //    Card card2 = new Card("Name", "Tehreem");
        //  cardList.add(card1);
        // cardList.add(card2);


        //ArrayAdapter<String> nameAdapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, nameList);


        Button btnONOFF = (Button) findViewById(R.id.btnONOFF);
        btnEnableDisable_Discoverable = (Button) findViewById(R.id.btnDiscoverable_on_off);
        lvNewDevices = (ListView) findViewById(R.id.lvNewDevices);
        mBTDevices = new ArrayList<>();
        btnStartConnection = (Button) findViewById(R.id.btnStartConnection);
        btnSend2 = (Button) findViewById(R.id.btnSend2);
        etSend2 = (EditText) findViewById(R.id.editText2);
        messages = new StringBuilder();

        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, new IntentFilter("incomingMessage"));


        //Broadcasts when bond state changes (ie:pairing)
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        registerReceiver(mBroadcastReceiver4, filter);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        lvNewDevices.setOnItemClickListener(MainActivity.this);


        btnONOFF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: enabling/disabling bluetooth.");
                enableDisableBT();
            }
        });

        btnStartConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startConnection();
            }
        });

      /*  btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                byte[] bytes = etSend.getText().toString().getBytes(Charset.defaultCharset());
                mBluetoothConnection.write(bytes);

                etSend.setText("");

            }
        });*/
        cardListAdapter = new CardAdapter(MainActivity.this, R.layout.card_adapter_view, cardList);
//        listNames.setAdapter(cardListAdapter);


//        FirebaseDatabase database = FirebaseDatabase.getInstance();
//        DatabaseReference ref = database.getReference().child(CUSTOMERS).child(mAuth.getCurrentUser().getUid());
//
////// Attach a listener to read the data at our posts reference
////        ref.addChildEventListener(new FireBaseChildListenerHelper(new IFirebaseChildClassListener() {
////            @Override
////            public void onChildAdded(Cards card) {
////                Log.i("onChildAdded: ", card.getName());
////                Toast.makeText(MainActivity.this, card.getName(), Toast.LENGTH_SHORT).show();
////
////            }
////        }));//run this code
//        ref.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                Customer customer = dataSnapshot.getValue(Customer.class);//error yaha hai na wo customer pick nai kr raha
//                Toast.makeText(MainActivity.this, customer.getName(), Toast.LENGTH_LONG).show();
//                System.out.println(customer);
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//                System.out.println("The read failed: " + databaseError.getCode());
//            }
//        });
//


        //   walletAdapter = new CardAdapter(MainActivity.this, R.layout.card_adapter_view, walletList);
        // wallet = (ListView) findViewById(R.id.wallet);
        //    wallet.setAdapter(walletAdapter);

        // Log.d("cardList", cardList.get(0).getText(0).getValue());
        //   btnSend2.setOnClickListener(new View.OnClickListener() {
        //        @Override
        //     public void onClick(View view) {


        //ArrayAdapter<Card> cardAdapter = new ArrayAdapter<Card>(MainActivity.this, android.R.layout.simple_list_item_1, cardList);


        //listNames.setAdapter(cardAdapter);

        // String temp_name = etSend2.getText().toString();
        // nameList.add(temp_name);











//
//
//
//
//
//        listNames.addOnItemTouchListener(
//                new RecyclerItemClickListener(this, listNames ,new RecyclerItemClickListener.OnItemClickListener() {
//                    @Override public void onItemClick(View view, int position) {
//
////inside onclick
//                        //CharSequence text = "Hello "+cardList.get(position);
//                        Log.d(TAG, "clicking");
//
//                        try {
//                            //   bytes = serialize();
//                            byte[] bytes = new byte[0];
//                            bytes = serialize_to_bytes(cardsList.get(position).getCard_ID());
//                            mBluetoothConnection.write(bytes);
//                            Log.d(TAG, "broken");
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//
//                        etSend2.setText("");
//
//
//                        //end onclick
//                    }
//
//
//                    @Override public void onLongItemClick(View view, int position) {
//                        // do whatever
//                    }
//                })
//        );
//
//
//
//
//









//        listNames.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                //CharSequence text = "Hello "+cardList.get(position);
//                Log.d(TAG, "clicking");
//
//                try {
//                    //   bytes = serialize();
//                    byte[] bytes = new byte[0];
//                    bytes = serialize_to_bytes(cardList.get(position));
//                    mBluetoothConnection.write(bytes);
//                    Log.d(TAG, "broken");
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }

//                etSend2.setText("");
//
//
//            }
//        });

//                etSend2.setText("");

        //   }
        // });


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public static final String UUID_ONE = "uzVZGmgn5UWa9fBPuU9IkHEtxus1";

    private void CheckLoginStatus() {

//        Log.i("auth", mAuth.getCurrentUser().getUid());

        if (SharedPreferneceHelper.getInstance(this).getString(USER_NAME).isEmpty()) {
            Intent i = new Intent(MainActivity.this, option_signup_login.class);
            startActivity(i);
        } else {
            Log.i(TAG, "vhgcgfc" + SharedPreferneceHelper.getInstance(this).getString(USER_NAME));
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        //FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();

        if (id == R.id.nav_first_layout) {


            fragmentManager.popBackStack();
            fragmentManager.popBackStack();


        } else if (id == R.id.nav_second_layout) {


            fragmentManager.popBackStack();


            //category_cont is a linear layout container for my fragment
            ft.replace(R.id.content_frame, new SecondFragment()).addToBackStack("tag");
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            ft.commit();

        } else if (id == R.id.nav_wallet) {


            fragmentManager.popBackStack();

            Bundle bundle = new Bundle();
            bundle.putSerializable("wallet_array", walletList);

            // set Fragmentclass Arguments
            WallerFragment fragobj = new WallerFragment();
            fragobj.setArguments(bundle);
            ft.replace(R.id.content_frame, new WallerFragment()).addToBackStack("tag");
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            ft.commit();

        } else if (id == R.id.nav_logout) {
            SharedPreferneceHelper.getInstance(this).clear();
            Intent intent = new Intent(this, option_signup_login.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Card getting = (Card) intent.getSerializableExtra("theCard");

            walletList.add(getting);

            Log.d("card", getting.getText(0).getValue());


            ((TextView) findViewById(R.id.item_base_lang).findViewById(R.id.cardName)).setText(getting.getText(0).getValue());
            ((TextView) findViewById(R.id.item_base_lang).findViewById(R.id.cardPhone)).setText(getting.getText(1).getValue());
            ((TextView) findViewById(R.id.item_base_lang).findViewById(R.id.cardEmail)).setText(getting.getText(2).getValue());
            ((TextView) findViewById(R.id.item_base_lang).findViewById(R.id.cardAddress)).setText(getting.getText(3).getValue());
            findViewById(R.id.item_base_lang).findViewById(R.id.cardView).setBackgroundColor(getting.getColor_fill());


            Log.d("card", getting.getText(0).getKey());
        }
    };

    //create method for starting connection
//***remember the conncction will fail and app will crash if you haven't paired first
    public void startConnection() {
        startBTConnection(mBTDevice, MY_UUID_INSECURE);
        myCardAdapter.setBluetoothConnectionService(mBluetoothConnection);
    }

    /**
     * starting chat service method
     */
    public void startBTConnection(BluetoothDevice device, UUID uuid) {
        Log.d(TAG, "startBTConnection: Initializing RFCOM Bluetooth Connection.");

        mBluetoothConnection.startClient(device, uuid);
        g.setmBluetoothAdapter(mBluetoothConnection);


    }


    public void disableBT() {
    }


    public void enableDisableBT() {
        if (mBluetoothAdapter == null) {
            Log.d(TAG, "enableDisableBT: Does not have BT capabilities.");
        }
        if (!mBluetoothAdapter.isEnabled()) {
            Log.d(TAG, "enableDisableBT: enabling BT.");
            Intent enableBTIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivity(enableBTIntent);

            IntentFilter BTIntent = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
            registerReceiver(mBroadcastReceiver1, BTIntent);
        }
        if (mBluetoothAdapter.isEnabled()) {
            Log.d(TAG, "enableDisableBT: disabling BT.");
            mBluetoothAdapter.disable();

            IntentFilter BTIntent = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
            registerReceiver(mBroadcastReceiver1, BTIntent);
        }

    }


    public void btnEnableDisable_Discoverable(View view) {
        Log.d(TAG, "btnEnableDisable_Discoverable: Making device discoverable for 300 seconds.");

        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
        startActivity(discoverableIntent);

        IntentFilter intentFilter = new IntentFilter(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED);
        registerReceiver(mBroadcastReceiver2, intentFilter);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void btnDiscover(View view) {
        Log.d(TAG, "btnDiscover: Looking for unpaired devices.");

        if (mBluetoothAdapter.isDiscovering()) {
            mBluetoothAdapter.cancelDiscovery();
            Log.d(TAG, "btnDiscover: Canceling discovery.");

            //check BT permissions in manifest
            checkBTPermissions();

            mBluetoothAdapter.startDiscovery();
            IntentFilter discoverDevicesIntent = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            registerReceiver(mBroadcastReceiver3, discoverDevicesIntent);
        }
        if (!mBluetoothAdapter.isDiscovering()) {

            //check BT permissions in manifest
            checkBTPermissions();

            mBluetoothAdapter.startDiscovery();
            IntentFilter discoverDevicesIntent = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            registerReceiver(mBroadcastReceiver3, discoverDevicesIntent);
        }

        Log.d("Discovery:", "taz, complete");
    }

    /**
     * This method is required for all devices running API23+
     * Android must programmatically check the permissions for bluetooth. Putting the proper permissions
     * in the manifest is not enough.
     * <p>
     * NOTE: This will only execute on versions > LOLLIPOP because it is not needed otherwise.
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void checkBTPermissions() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            int permissionCheck = this.checkSelfPermission("Manifest.permission.ACCESS_FINE_LOCATION");
            permissionCheck += this.checkSelfPermission("Manifest.permission.ACCESS_COARSE_LOCATION");
            if (permissionCheck != 0) {

                this.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001); //Any number
            }
        } else {
            Log.d(TAG, "checkBTPermissions: No need to check permissions. SDK version < LOLLIPOP.");
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        //first cancel discovery because its very memory intensive.
        mBluetoothAdapter.cancelDiscovery();

        Log.d(TAG, "onItemClick: You Clicked on a device.");
        String deviceName = mBTDevices.get(i).getName();
        String deviceAddress = mBTDevices.get(i).getAddress();

        Log.d(TAG, "onItemClick: deviceName = " + deviceName);
        Log.d(TAG, "onItemClick: deviceAddress = " + deviceAddress);

        //create the bond.
        //NOTE: Requires API 17+? I think this is JellyBean
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2) {
            Log.d(TAG, "Trying to pair with " + deviceName);
            mBTDevices.get(i).createBond();

            mBTDevice = mBTDevices.get(i);
            mBluetoothConnection = new BluetoothConnectionService(MainActivity.this);
        }
    }



    public static byte[] serialize_to_bytes(Object obj) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(out);
        os.writeObject(obj);
        return out.toByteArray();
    }

    public void add_card(View view) {
        //    Intent intent = new Intent(this, form_add_card.class);
        //  startActivity(intent);
    }

    /*public void open_wallet(View view){
        Intent intent = new Intent(this, my_wallet.class);
        intent.putExtra("wallet_array", walletList);
        startActivity(intent);
    }*/



    public void add(View view) {


        EditText nameText = (EditText) findViewById(R.id.name);
        String name = nameText.getText().toString();

        EditText phoneText = (EditText) findViewById(R.id.phone);
        String phone = phoneText.getText().toString();

        EditText emailText = (EditText) findViewById(R.id.email);
        String email = emailText.getText().toString();


        EditText addressText = (EditText) findViewById(R.id.address);
        String address = addressText.getText().toString();

        Spinner color = (Spinner) findViewById(R.id.spinner);
        String card_color = (String) color.getSelectedItem();



     /*   array_text[0]= new String[]{"name", name};
        array_text[1]= new String[]{"phone", phone};
        array_text[2]= new String[]{"email", email};
        array_text[3]= new String[]{"address", address};*/


        Text array_text1 = new Text("name", name);
        Text array_text2 = new Text("phone", phone);
        Text array_text3 = new Text("email", email);
        Text array_text4 = new Text("address", address);

        Log.d("mm", "jjklj");

        Text[] texts = new Text[4];
        texts[0] = array_text1;
        texts[1] = array_text2;
        texts[2] = array_text3;
        texts[3] = array_text4;


        Log.d("mm", "bhj");
        Card card = new Card(texts, card_color);
        cardList.add(card);


        Log.i("yay", "hbghyj");


        //listNames.
        //listNames.refreshDrawableState();

        cardListAdapter.notifyDataSetChanged();

        //ListView listCards = (ListView) findViewById(R.id.listNames);
        //listCards.notif
        //listNames.notifyDataSetChanged();
        // cardA
        fragmentManager.popBackStack();
        //findViewById(R.id.)


        // Intent intent = new Intent(this, MainActivity.class);
        //intent.putExtra("mycard", card);
        //   startActivity(intent);
    }

    public ArrayList<Card> getWalletList() {
        return walletList;
    }

    public BluetoothConnectionService getService() {
        return mBluetoothConnection;
    }

}
