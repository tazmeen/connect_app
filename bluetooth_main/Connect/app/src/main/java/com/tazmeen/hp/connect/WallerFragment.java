package com.tazmeen.hp.connect;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import static com.tazmeen.hp.connect.R.id.wallet;

/**
 * Created by hp on 12/16/2017.
 */

public class WallerFragment extends Fragment {


    ListView wallet;
    //public final static ArrayList<Card> walletList = new ArrayList<Card>();

    public CardAdapter walletAdapter;



    View myView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.waller, container, false);
//final ArrayList<Card> wallet_List = (ArrayList<Card>) i.getSerializableExtra("wallet_array");


        MainActivity activity = (MainActivity) getActivity();
        final ArrayList<Card> wallet_List = activity.getWalletList();
        int size = wallet_List.size();

        BluetoothConnectionService mConnection = activity.getService();

                //final ArrayList<Card> wallet_List = getArguments().getSerializable("wallet_array");
        walletAdapter = new CardAdapter(getActivity(), R.layout.card_adapter_view, wallet_List);
        wallet = (ListView) myView.findViewById(R.id.wallet);
        wallet.setAdapter(walletAdapter);
        return myView;

    }
}
