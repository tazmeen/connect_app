package com.tazmeen.hp.connect.listeners;

import com.tazmeen.hp.connect.models.EmployeeCustomer;

/**
 * Created by Ansar Shahid on 3/17/2018.
 */

@FunctionalInterface
public interface IFireBaseRealTimeListener {

    void onChildAdded(EmployeeCustomer employeeCustomer);
}
