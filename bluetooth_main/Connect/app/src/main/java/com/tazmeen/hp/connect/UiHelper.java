package com.tazmeen.hp.connect;

import android.app.Activity;
import android.support.annotation.NonNull;

import com.afollestad.materialdialogs.MaterialDialog;

/**
 * Created by Ansar Shahid on 3/16/2018.
 */

public class UiHelper {

    private static UiHelper UI_HELPER = null;

    private UiHelper() {

    }

    public static UiHelper getInstance() {
        if (UI_HELPER == null)
            UI_HELPER = new UiHelper();
        return UI_HELPER;
    }

    public MaterialDialog getCircularProgress(@NonNull final Activity activity, @NonNull String content) {
        return new MaterialDialog.Builder(activity)
                .progress(true, 100)
                .cancelable(false)
                .content(content)
                .show();
    }
}
