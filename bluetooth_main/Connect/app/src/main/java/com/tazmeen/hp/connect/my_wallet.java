package com.tazmeen.hp.connect;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;

public class my_wallet extends AppCompatActivity {

    ListView wallet;
    //public final static ArrayList<Card> walletList = new ArrayList<Card>();

    public CardAdapter walletAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_wallet);


        Intent i = getIntent();
        final ArrayList<Card> wallet_List = (ArrayList<Card>) i.getSerializableExtra("wallet_array");

        walletAdapter = new CardAdapter(my_wallet.this, R.layout.card_adapter_view, wallet_List);
        wallet = (ListView) findViewById(R.id.wallet);
        wallet.setAdapter(walletAdapter);
    }


}
