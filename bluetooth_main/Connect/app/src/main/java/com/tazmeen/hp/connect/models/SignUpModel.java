package com.tazmeen.hp.connect.models;

/**
 * Created by Ansar Shahid on 3/16/2018.
 */

public class SignUpModel {

    private String companyName;
    private String username;
    private String email;
    private String password;
    private String phone;
    private int planAmount;

    public SignUpModel(String companyName, String username, String email, String password, String phone) {
        this.companyName = companyName;
        this.username = username;
        this.email = email;
        this.password = password;
        this.phone = phone;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
