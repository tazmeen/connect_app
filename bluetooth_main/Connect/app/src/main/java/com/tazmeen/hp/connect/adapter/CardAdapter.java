package com.tazmeen.hp.connect.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.tazmeen.hp.connect.BluetoothConnectionService;
import com.tazmeen.hp.connect.Globals;
import com.tazmeen.hp.connect.MainActivity;
import com.tazmeen.hp.connect.R;
import com.tazmeen.hp.connect.listeners.IItemClickListener;
import com.tazmeen.hp.connect.models.Cards;
import com.tazmeen.hp.connect.viewHolders.CardViewHolder;

import java.io.IOException;
import java.util.List;

import static com.tazmeen.hp.connect.MainActivity.serialize_to_bytes;

/**
 * Created by hp on 3/19/2018.
 */

public class CardAdapter extends RecyclerView.Adapter<CardViewHolder> {

    private List<Cards> cardsList;
    Globals g;
    BluetoothConnectionService bcs;
    private LayoutInflater layoutInflater;
    private Picasso picasso;

    public CardAdapter(List<Cards> cards, final Context context, final Picasso picasso) {
        this.cardsList = cards;
        this.layoutInflater = LayoutInflater.from(context);
        this.picasso = picasso;
    }

    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CardViewHolder(layoutInflater.inflate(R.layout.single_card_view, parent, false));
    }

//    public void bind(final ContentItem item, final IItemClickListener listener) {
//
//        itemView.setOnClickListener(new View.OnClickListener() {
//            @Override public void onClick(View v) {
//                listener.onItemClick(getAdapterPosition);
//            }
//        });
//    }

    @Override
    public void onBindViewHolder(@NonNull CardViewHolder holder, int position) {
        Cards cards = cardsList.get(position);
        picasso.load(cards.getUri()).into(holder.getSingleCardImageView());
        holder.setItemClickListener(new IItemClickListener() {
            @Override
            public void onItemClick(int position) {

                Cards c = cardsList.get(position);
                String id = c.getCard_ID();
                try {

               //     Intent intent = new Intent(CardAdapter.this, MainActivity.class);
               //     Intent intent = new Intent(this, MainActivity.class);

//                    intent.putExtra("arg", c.getCard_ID()); // getText() SHOULD NOT be static!!!

  //                  startActivity(intent);
                    //   bytes = serialize();
                    byte[] bytes = new byte[0];
                    bytes = serialize_to_bytes(id);
                    bcs.write(bytes);
                    Log.d("TAG", "broken");
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Log.i("A", c.getEmail());



            }
        });
        Log.i("TAAGGG", cards.getEmail());
    }

    @Override
    public int getItemCount() {
        return cardsList.size();
    }

    public void setBluetoothConnectionService(BluetoothConnectionService bcs){
        this.bcs = bcs;
    }
}
