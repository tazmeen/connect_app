package com.tazmeen.hp.connect.firebase;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.tazmeen.hp.connect.models.Cards;

/**
 * Created by hp on 3/19/2018.
 */

public class CardReadListeneerHelper implements ChildEventListener {

    private IFireBaseCardReadListener firebaseCardReadListener;

    public CardReadListeneerHelper(IFireBaseCardReadListener fireBaseCardReadListener) {
        this.firebaseCardReadListener = fireBaseCardReadListener;
    }

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        Cards cards = dataSnapshot.getValue(Cards.class);
        firebaseCardReadListener.onCardAdded(cards);
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }
}
