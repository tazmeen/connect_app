package com.tazmeen.hp.connect.app;

import android.app.Application;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;

/**
 * Created by hp on 3/19/2018.
 */

public class MyApplication extends Application {

    private Picasso picasso;

    @Override
    public void onCreate() {
        super.onCreate();
        File file = new File(getFilesDir(), "images_dir");
        if (!file.exists())
            file.mkdirs();
        Cache cache = new Cache(file, 10 * 1000 * 1000); // 10 Mib
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .writeTimeout(15, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .connectTimeout(15, TimeUnit.SECONDS)
                .cache(cache)
                .build();
        picasso = new Picasso.Builder(this)
                .loggingEnabled(true)
                .downloader(new OkHttp3Downloader(okHttpClient))
                .build();
    }

    public Picasso getPicasso() {
        return Picasso.get();
    }
}
