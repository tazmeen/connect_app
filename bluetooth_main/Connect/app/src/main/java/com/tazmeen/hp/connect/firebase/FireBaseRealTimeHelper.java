package com.tazmeen.hp.connect.firebase;

import com.tazmeen.hp.connect.models.Customer;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Ansar Shahid on 3/17/2018.
 */

public class FireBaseRealTimeHelper {

    private static final String CUSTOMERS = "customers";
    public static final String EMPLOYEES = "employees";
    private final DatabaseReference customerDatabaseReference;
    private final DatabaseReference employeeDatabaseReference;

    public FireBaseRealTimeHelper() {
        customerDatabaseReference = FirebaseDatabase.getInstance().getReference().child(CUSTOMERS);
        employeeDatabaseReference = FirebaseDatabase.getInstance().getReference().child(EMPLOYEES);
    }

    public void pushData(Customer customer) {
        customerDatabaseReference.child(customer.getUuid())
                .setValue(customer);
    }

//    public void pushData(Employee employee) {
//        employeeDatabaseReference.child(employee.uuid())
//                .setValue(employee);
//    }

}
