package com.tazmeen.hp.connect.firebase;

import com.tazmeen.hp.connect.models.Cards;
import com.tazmeen.hp.connect.models.Customer;

/**
 * Created by hp on 3/18/2018.
 */

public interface IFirebaseChildClassListener {
    void onChildAdded(Cards card);
}
