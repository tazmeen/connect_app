package com.tazmeen.hp.connect.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.tazmeen.hp.connect.R;
import com.tazmeen.hp.connect.listeners.IItemClickListener;
import com.tazmeen.hp.connect.models.EmployeeCustomer;
import com.tazmeen.hp.connect.viewHolders.AddUserViewHolder;

import java.util.List;

/**
 * Created by Ansar Shahid on 3/17/2018.
 */

public class AddUserAdapter extends RecyclerView.Adapter<AddUserViewHolder> {

    private final List<EmployeeCustomer> customerList;
    private final LayoutInflater layoutInflater;
    private IAddUserClickListener iAddUserClickListener;

    public AddUserAdapter(List<EmployeeCustomer> customerList, final Context context) {
        this.customerList = customerList;
        this.layoutInflater = LayoutInflater.from(context);
        if (context instanceof IAddUserClickListener)
            iAddUserClickListener = (IAddUserClickListener) context;
        else
            throw new RuntimeException("Parent class does not implement IAddUserClickListener.");
    }

    @NonNull
    @Override
    public AddUserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AddUserViewHolder(layoutInflater.inflate(R.layout.add_user_single_view, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final AddUserViewHolder holder, int position) {
        EmployeeCustomer employeeCustomer = customerList.get(position);
        holder.getTvEmail().setText(employeeCustomer.getEmail());
        holder.getTvUsername().setText(employeeCustomer.getName());
        holder.setItemClickListener(new IItemClickListener() {
            @Override
            public void onItemClick(int position) {
                iAddUserClickListener.onUserAdd(customerList.get(position));
                holder.getAddUserImageView().setImageResource(R.drawable.tick_vector_icon);
            }
        });
    }

    @Override
    public int getItemCount() {
        return customerList.size();
    }

    @FunctionalInterface
    public interface IAddUserClickListener {
        void onUserAdd(EmployeeCustomer employeeCustomer);
    }
}
