package com.tazmeen.hp.connect;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.tazmeen.hp.connect.utils.SharedPreferneceHelper;

import static com.tazmeen.hp.connect.Constants.USER_NAME;

public class sign_up extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up); TextView userNameTextView = findViewById(R.id.userNameTextView);
        findViewById(R.id.logOutButton).setOnClickListener(this);
        findViewById(R.id.addUserImageView).setOnClickListener(this);
        userNameTextView.setText(SharedPreferneceHelper.getInstance(this).getString(USER_NAME));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.logOutButton: {
                SharedPreferneceHelper.getInstance(this).clear();
                Intent intent = new Intent(this, SignUpActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
            }
            case R.id.addUserImageView: {
                startActivity(new Intent(this, AddUserActivity.class));
                break;
            }
        }
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK: {
                moveTaskToBack(true);
                return true;
            }
        }
        return false;
    }
}
