package com.tazmeen.hp.connect.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tazmeen.hp.connect.R;
import com.tazmeen.hp.connect.listeners.IItemClickListener;

/**
 * Created by Ansar Shahid on 3/17/2018.
 */

public class AddUserViewHolder extends RecyclerView.ViewHolder {

    private TextView tvEmail, tvUsername;
    private ImageView addUserImageView;
    private IItemClickListener iItemClickListener;

    public AddUserViewHolder(View itemView) {
        super(itemView);
        tvEmail = itemView.findViewById(R.id.tvEmail);
        tvUsername = itemView.findViewById(R.id.tvUsername);
        addUserImageView = itemView.findViewById(R.id.addUserImageView);
        addUserImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iItemClickListener.onItemClick(getAdapterPosition());
            }
        });
    }

    public void setItemClickListener(IItemClickListener iItemClickListener) {
        this.iItemClickListener = iItemClickListener;
    }

    public TextView getTvEmail() {
        return tvEmail;
    }

    public TextView getTvUsername() {
        return tvUsername;
    }

    public ImageView getAddUserImageView() {
        return addUserImageView;
    }
}
