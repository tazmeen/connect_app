package com.example.hp.application;

import android.net.Uri;

/**
 * Created by hp on 3/14/2018.
 */

public interface IImageUploadListener {

    void onImageUpload(Uri uri);

    void onImageError(Exception e);
}
