package com.example.hp.application;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;

public class MainActivity extends AppCompatActivity implements IFirebaseListener {

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.textView);
        User user = new User("Temp", "5345534", "temp@gmail.com");
//        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("users");
//        databaseReference.child(user.getContact()).setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
//            @Override
//            public void onComplete(@NonNull Task<Void> task) {
//              if(task.isSuccessful()){
//                  Toast.makeText(MainActivity.this, "Uploaded", Toast.LENGTH_SHORT).show();
//              }else
//                  if(task.getException() != null) {
//                      Log.e("onComplete: ", task.getException().getMessage());
//                      task.getException().printStackTrace();
//                  }
//
//            }
//        });
//
//        databaseReference.addChildEventListener(new FirebaseDatabaseHelper(this));
//
//        DBHelp.with(this).putString(USER_NAME,"Temp");
//        String name = DBHelp.with(this).getString(USER_NAME);
//
//        databaseReference.child("users").child("temp@gmil.com")
//                .setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
//            @Override
//            public void onComplete(@NonNull Task<Void> task) {
//
//            }
//        });
        final ImageView imageView = (ImageView) findViewById(R.id.imageView);


        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.capture);
        new FirebaseStorageHelper().uploadPhoto(getImageUrl(bitmap), new IImageUploadListener() {
            @Override
            public void onImageUpload(Uri uri) {
                Picasso.with(MainActivity.this).load(uri).into(imageView);
            }

            @Override
            public void onImageError(Exception e) {
                e.printStackTrace();
            }
        });
    }

    private byte[] getImageUrl(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    @Override
    public void onChildAdded(User user) {
        textView.setText(user.getName());
    }

    @Override
    public void onChildChanged(User user) {
        textView.setText(user.getName());
    }



}