package com.example.hp.application;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;



public class DBHelp {

    private static TinyDB db = new TinyDB();

    public static TinyDB with(Context c) {
        db.setContext(c);
        return db;
    }

    public static class TinyDB {

        private SharedPreferences preferences;

        TinyDB() {

        }

        public void setContext(Context appContext) {
            preferences = PreferenceManager.getDefaultSharedPreferences(appContext);
        }

        /**
         * Get String value from SharedPreferences at 'key'. If key not found, return ""
         *
         * @param key SharedPreferences key
         * @return String value at 'key' or "" (empty String) if key not found
         */
        public String getString(String key) {
            return preferences.getString(key, "");
        }

        /**
         * Put String value into SharedPreferences with 'key' and save
         *
         * @param key   SharedPreferences key
         * @param value String value to be added
         */
        public void putString(String key, String value) {
            preferences.edit().putString(key, value).apply();
        }

        public void putInt(String key,int value){
            preferences.edit().putInt(key, value).apply();
        }

        public void clearSharedPref() {
            preferences.edit().clear().apply();
        }

    }
}