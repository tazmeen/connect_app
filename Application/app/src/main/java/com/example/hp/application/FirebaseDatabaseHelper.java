package com.example.hp.application;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

/**
 * Created by hp on 3/14/2018.
 */

public class FirebaseDatabaseHelper implements ChildEventListener{

    private final IFirebaseListener iFirebaseListener;

    public FirebaseDatabaseHelper(IFirebaseListener iFirebaseListener) {
        this.iFirebaseListener = iFirebaseListener;
    }

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        User user = dataSnapshot.getValue(User.class);
        iFirebaseListener.onChildAdded(user);
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
        User user = dataSnapshot.getValue(User.class);
        iFirebaseListener.onChildChanged(user);
    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }
}
