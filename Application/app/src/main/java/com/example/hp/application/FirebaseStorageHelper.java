package com.example.hp.application;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;

/**
 * Created by hp on 3/14/2018.
 */

public class FirebaseStorageHelper {

    public void uploadPhoto(byte[] bytes, final IImageUploadListener iImageUploadListener){
        FirebaseStorage.getInstance().getReference().child("images").putBytes(bytes)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        if(taskSnapshot.getError() != null) {
                            Uri uri = taskSnapshot.getDownloadUrl();
                            iImageUploadListener.onImageUpload(uri);
                        }
                    }
                });
    }
}
